﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApiExample
{
    class ContextoDB : DbContext
    {
        public DbSet<Persona> Personas { get; set; }
        public DbSet<Pasaporte> Pasaportes { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Proyecto> Proyectos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Agregando Cedula como clave primaria
            modelBuilder.Entity<Persona>().HasKey(p => p.Cedula);

            //Agrego el nombre como campo requerido
            modelBuilder.Entity<Persona>().Property(p => p.Nombre).IsRequired();

            //Pasaporte opcional para persona, pero persona requerida para pasaporte
            modelBuilder.Entity<Pasaporte>().HasRequired(p => p.Persona).WithOptional(p => p.Pasaporte)
                .Map(mc => mc.MapKey("Persona"));

            //Empresa tiene clave primaria RUT
            modelBuilder.Entity<Empresa>().HasKey(e => e.RUT);

            //Configurando relación 1 a N con requerido del lado del uno.
            modelBuilder.Entity<Empresa>().HasMany<Persona>(e => e.Empleados).WithRequired();

            //Cambiar el nombre de una tabla
            modelBuilder.Entity<Proyecto>().ToTable("Proyectos");

            //Asociando proyecto padre con subproyectos 
            modelBuilder.Entity<Proyecto>()
                .HasOptional<Proyecto>(p => p.ProyectoPadre)
                .WithMany(p => p.SubProyectos)
                .Map(mc => mc.MapKey("Padre"));

            //Asociando proyecto siguiente.
            modelBuilder.Entity<Proyecto>()
                .HasMany<Proyecto>(p => p.SiguientesProyectos)
                .WithOptional().Map(mc => mc.MapKey("Anterior"));

            //Many to many
            modelBuilder.Entity<Empresa>().HasMany<Proyecto>(e => e.Proyectos).WithMany()
                .Map(mc =>
                {
                    mc.ToTable("ProyectosDeEmpresa");
                    mc.MapLeftKey("Empresa");
                    mc.MapRightKey("Proyecto");
                });


            base.OnModelCreating(modelBuilder);
        }

    }
}
