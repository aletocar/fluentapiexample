namespace FluentApiExample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class proyectoEmpresas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProyectosDeEmpresa",
                c => new
                    {
                        Empresa = c.Int(nullable: false),
                        Proyecto = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Empresa, t.Proyecto })
                .ForeignKey("dbo.Empresas", t => t.Empresa, cascadeDelete: true)
                .ForeignKey("dbo.Proyectos", t => t.Proyecto, cascadeDelete: true)
                .Index(t => t.Empresa)
                .Index(t => t.Proyecto);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProyectosDeEmpresa", "Proyecto", "dbo.Proyectos");
            DropForeignKey("dbo.ProyectosDeEmpresa", "Empresa", "dbo.Empresas");
            DropIndex("dbo.ProyectosDeEmpresa", new[] { "Proyecto" });
            DropIndex("dbo.ProyectosDeEmpresa", new[] { "Empresa" });
            DropTable("dbo.ProyectosDeEmpresa");
        }
    }
}
