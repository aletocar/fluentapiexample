namespace FluentApiExample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresas",
                c => new
                    {
                        RUT = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.RUT);
            
            CreateTable(
                "dbo.Personas",
                c => new
                    {
                        Cedula = c.String(nullable: false, maxLength: 128),
                        Nombre = c.String(nullable: false),
                        Apellido = c.String(),
                        Empresa_RUT = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Cedula)
                .ForeignKey("dbo.Empresas", t => t.Empresa_RUT, cascadeDelete: true)
                .Index(t => t.Empresa_RUT);
            
            CreateTable(
                "dbo.Pasaportes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nacionalidad = c.String(),
                        Persona = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Personas", t => t.Persona)
                .Index(t => t.Persona);
            
            CreateTable(
                "dbo.Proyectos",
                c => new
                    {
                        ProyectoId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(),
                        FechaComienzo = c.DateTime(nullable: false),
                        Padre = c.Int(),
                        Proyecto_ProyectoId = c.Int(),
                    })
                .PrimaryKey(t => t.ProyectoId)
                .ForeignKey("dbo.Proyectos", t => t.Padre)
                .ForeignKey("dbo.Proyectos", t => t.Proyecto_ProyectoId)
                .Index(t => t.Padre)
                .Index(t => t.Proyecto_ProyectoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Proyectos", "Proyecto_ProyectoId", "dbo.Proyectos");
            DropForeignKey("dbo.Proyectos", "Padre", "dbo.Proyectos");
            DropForeignKey("dbo.Personas", "Empresa_RUT", "dbo.Empresas");
            DropForeignKey("dbo.Pasaportes", "Persona", "dbo.Personas");
            DropIndex("dbo.Proyectos", new[] { "Proyecto_ProyectoId" });
            DropIndex("dbo.Proyectos", new[] { "Padre" });
            DropIndex("dbo.Pasaportes", new[] { "Persona" });
            DropIndex("dbo.Personas", new[] { "Empresa_RUT" });
            DropTable("dbo.Proyectos");
            DropTable("dbo.Pasaportes");
            DropTable("dbo.Personas");
            DropTable("dbo.Empresas");
        }
    }
}
