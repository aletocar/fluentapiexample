namespace FluentApiExample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class proyectoAnterior : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Proyectos", name: "Proyecto_ProyectoId", newName: "Anterior");
            RenameIndex(table: "dbo.Proyectos", name: "IX_Proyecto_ProyectoId", newName: "IX_Anterior");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Proyectos", name: "IX_Anterior", newName: "IX_Proyecto_ProyectoId");
            RenameColumn(table: "dbo.Proyectos", name: "Anterior", newName: "Proyecto_ProyectoId");
        }
    }
}
