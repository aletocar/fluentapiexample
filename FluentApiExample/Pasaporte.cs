﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApiExample
{
    public class Pasaporte
    {
        public int Id { get; set; }
        public string Nacionalidad { get; set; }
        public Persona Persona { get; set; }

        public Pasaporte() { }
    }
}
