﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApiExample
{
    public class Empresa
    {
        public int RUT { get; set; }
        public string Nombre { get; set; }
        public List<Persona> Empleados { get; set; }
        public List<Proyecto> Proyectos { get; set; }

        public Empresa()
        {
            Empleados = new List<Persona>();
            Proyectos = new List<Proyecto>();
        }
    }
}
