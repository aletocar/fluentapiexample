﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentApiExample
{
    public class Proyecto
    {
        public int ProyectoId { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaComienzo { get; set; }
        public Proyecto ProyectoPadre { get; set; }
        public List<Proyecto> SubProyectos { get; set; }
        public List<Proyecto> SiguientesProyectos { get; set; }

        public Proyecto()
        {
            SubProyectos = new List<Proyecto>();
            SiguientesProyectos = new List<Proyecto>();
        }
    }
}
